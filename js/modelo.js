var tablemodelo;
function listar_modelo(){
        tablemodelo = $("#tabla_modelo").DataTable({
        "ordering":true,
        "bLengthChange":false,
        "paging": true,
        "searching": { "regex": true },
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "pageLength": 10,
        "destroy":true,
        "async": false ,
        "processing": true,
        "ajax":{
            "url":"../controlador/modelo/controlador_modelo_listar.php",
            type:'POST'
        },
        "order":[[1, 'asc']],
        "columns":[
            {"defaultContent":""},
            {"data":"marca_nombre"},
            {"data":"modelo_nombre"},  
            {"defaultContent":"<button style='font-size:13px;' type='button' class='editar btn btn-primary'><i class='fa fa-edit'></i></button>"}
        ],

        "language":idioma_espanol,
        "columnDefs": [
                {className: "dt-center", "targets": [0,1, 2, 3]
                }
            ],
        select: true
    });
    document.getElementById("tabla_modelo_filter").style.display="none";
    $('input.global_filter').on('keyup click',function(){
        filterGlobal();
    });
    $('input.column_filter').on('keyup click',function(){
        filterColumn($(this).parents('tr').attr('data-column'));
    });

    tablemodelo.on('draw.dt', function(){
        var PageInfo = $('#tabla_modelo').DataTable().page.info();
        tablemodelo.column(0, { page: 'current'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

}

$('#tabla_modelo').on('click','.editar',function(){
    var data = tablemodelo.row($(this).parents('tr')).data();//Detecta a que fila hago click y captura los datos en la variable data
    if(tablemodelo.row(this).child.isShown()){//Cuando esta en tamaño responsivo
        var data = tablemodelo.row(this).data();
    }
    $("#modal_editar").modal({backdrop:'static',keyboard:false});
    $("#modal_editar").modal('show');
    $("#id_modelo").val(data.modelo_id);
    $("#txt_modelo_actual_editar").val(data.modelo_nombre);
    $("#txt_modelo_nuevo_editar").val(data.modelo_nombre);
    $("#cbm_marca_editar").val(data.marca_id).trigger("change");
})

function LimpiarCampos(){
    $("#txt_modelo").val("");
}

function filterGlobal(){
    $('#tabla_modelo').DataTable().search(
        $('#global_filter').val(),
    ).draw();
}

function listar_combo_rol(){
    $.ajax({
        "url":"../controlador/usuario/controlador_combo_rol_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                if(data[i][0]=='3'){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
                }
            }
            $("#cbm_rol").html(cadena);
            $("#cbm_rol_editar").html(cadena);
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_rol").html(cadena);
            $("#cbm_rol_editar").html(cadena);
            
        }
    })
}

function listar_marca_combo(){
    $.ajax({
        "url":"../controlador/modelo/controlador_combo_marca_listar.php",
        type:'POST'
    }).done(function(resp){
        //alert(resp);
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_marca").html(cadena);
            $("#cbm_marca_editar").html(cadena);
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_marca").html(cadena);
            $("#cbm_marca_editar").html(cadena);
            
        }
    })
}

function Registrar_Modelo(){
    var modelo = $("#txt_modelo").val();
    var marca = $("#cbm_marca").val();
    if(modelo.length==0 || marca.length==0){
        return Swal.fire("Mensaje de advertencia", "Llene todos los campos", "warning");
    }
    $.ajax({
        "url":"../controlador/modelo/controlador_modelo_registro.php",
        type:'POST',
        data:{
            modelo:modelo,
            marca:marca
        }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                listar_modelo();
                LimpiarCampos();
                $("#modal_registro").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Modelo guardado", "success");
                }else{
                LimpiarCampos();
                Swal.fire("Mensaje de advertencia", "Ya existen", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}

function Editar_Modelo(){
    var idmodelo = $("#id_modelo").val();
    var modeloactual = $("#txt_modelo_actual_editar").val();
    var modelonuevo = $("#txt_modelo_nuevo_editar").val();
    var marca = $("#cbm_marca_editar").val();
    if(modeloactual.length==0 || modelonuevo.length==0 || marca.length==0){
        return Swal.fire("Mensaje de advertencia", "Llene todos los campos", "warning");
    }
    $.ajax({
        "url":"../controlador/modelo/controlador_modelo_modificar.php",
        type:'POST',
        data:{
            idmodelo:idmodelo,
            moac:modeloactual,
            monu:modelonuevo,
            marca:marca
        }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                $("#modal_editar").modal('hide');
                listar_modelo();
                Swal.fire("Mensaje de confirmacion", "Datos actualizados", "success");
                }else{
                LimpiarCampos();
                Swal.fire("Mensaje de advertencia", "Datos ya existen en la bd", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}