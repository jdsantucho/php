var tableestacion;
function listar_estacion(){
        tableestacion = $("#tabla_estacion").DataTable({
        "ordering":true,
        "bLengthChange":false,
        "paging": true,
        "searching": { "regex": true },
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "pageLength": 10,
        "destroy":true,
        "async": false ,
        "processing": true,
        "ajax":{
            "url":"../controlador/estacion/controlador_estacion_listar.php",
            type:'POST'
        },
        //"order":[[1, 'asc']],
        "columns":[
            {"defaultContent":""},
            {"data":"estacion_nombre"},
            {"defaultContent":"<button style='font-size:13px;' type='button' class='editar btn btn-primary'><i class='fa fa-edit'></i></button>"}
        ],

        "language":idioma_espanol,
        "columnDefs": [
                {className: "dt-center", "targets": [0,1, 2]
                }
            ],
        select: true
    });
    document.getElementById("tabla_estacion_filter").style.display="none";
    $('input.global_filter').on('keyup click',function(){
        filterGlobal();
    });
    $('input.column_filter').on('keyup click',function(){
        filterColumn($(this).parents('tr').attr('data-column'));
    });

    tableestacion.on('draw.dt', function(){
        var PageInfo = $('#tabla_estacion').DataTable().page.info();
        tableestacion.column(0, { page: 'current'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

}

function filterGlobal(){
    $('#tabla_estacion').DataTable().search(
        $('#global_filter').val(),
    ).draw();
}

function Registrar_Estacion(){
    var estacion = $("#txt_estacion").val();

    if(estacion.length==0){
        return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning");
    }

    $.ajax({
        "url":"../controlador/estacion/controlador_estacion_registro.php",
         type:'POST',
         data:{
            estacion:estacion
         }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                listar_estacion();
                LimpiarCampos();
                $("#modal_registro").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Estacion registrada", "success");
                }else{
                LimpiarCampos();
                Swal.fire("Mensaje de advertencia", "La estacion ya existe en la base de datos", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}

function LimpiarCampos(){
    $("#txt_estacion").val("");
}

$('#tabla_estacion').on('click','.editar',function(){
    var data = tableestacion.row($(this).parents('tr')).data();//Detecta a que fila hago click y captura los datos en la variable data
    if(tableestacion.row(this).child.isShown()){//Cuando esta en tamaño responsivo
        var data = tableestacion.row(this).data();
    }
    $("#modal_editar").modal({backdrop:'static',keyboard:false});
    $("#modal_editar").modal('show');
    $("#id_estacion").val(data.estacion_id);
    $("#txt_estacion_actual_editar").val(data.estacion_nombre);
    $("#txt_estacion_nueva_editar").val(data.estacion_nombre);
})

function Editar_Estacion(){
    var id = $("#id_estacion").val();
    var estacionactual = $("#txt_estacion_actual_editar").val();
    var estacionnueva = $("#txt_estacion_nueva_editar").val();
    if(estacionactual.length==0 || estacionnueva.length==0){
        return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning");
    }

    $.ajax({
        "url":"../controlador/estacion/controlador_estacion_modificar.php",
         type:'POST',
         data:{
            id:id,
            estacionac:estacionactual,
            estacionnu:estacionnueva
         },
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                $("#modal_editar").modal('hide');
                listar_estacion();
                Swal.fire("Mensaje de confirmacion", "Datos actualizados", "success");
                }else{
                Swal.fire("Mensaje de advertencia", "La estacion ya existe en la base de datos", "warning");
                }
        }else{
           
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}