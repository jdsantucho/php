var tablelspersonal;
function listar_lspersonal(){
        tablelspersonal = $("#tabla_lspersonal").DataTable({
        "ordering":true,
        "bLengthChange":false,
        "paging": true,
        "searching": { "regex": true },
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "pageLength": 10,
        "destroy":true,
        "async": false ,
        "processing": true,
        
        "ajax":{
            "url":"../controlador/lspersonal/controlador_lspersonal_listar.php",
            type:'POST'
        },
        "order":[[1, 'asc']],
        "columns":[
            {"defaultContent":""},
            {"data":"lspersonal_nombre"},
            {"data":"lspersonal_interno"},
            {"data":"departamento"},
            {"data":"gerencia"},
            {"data":"estacion"},
            {"data":"lspersonal_estatus",
            render: function (data, type, row ) {
                if(data=='ACTIVO'){
                    return "<span class='label label-primary'>"+data+"</span>";                   
                }if(data=='INACTIVO'){
                    return "<span class='label label-danger'>"+data+"</span>";                   
                }
              }
            },  
            {"defaultContent":"<button style='font-size:13px;' type='button' class='editar btn btn-primary'><i class='fa fa-edit'></i></button>"}
        ],

        "language":idioma_espanol,
        "columnDefs": [
                {className: "dt-center", "targets": [0,1, 2, 3,4, 5, 6, 7]
                }

            ],


        select: true
    });
    document.getElementById("tabla_lspersonal_filter").style.display="none";
    $('input.global_filter').on('keyup click',function(){
        filterGlobal();
    });
    $('input.column_filter').on('keyup click',function(){
        filterColumn($(this).parents('tr').attr('data-column'));
    });

    tablelspersonal.on('draw.dt', function(){
        var PageInfo = $('#tabla_lspersonal').DataTable().page.info();
        tablelspersonal.column(0, { page: 'current'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

}

function filterGlobal(){
    $('#tabla_lspersonal').DataTable().search(
        $('#global_filter').val(),
    ).draw();
}

$('#tabla_lspersonal').on('click','.editar',function(){
    var data = tablelspersonal.row($(this).parents('tr')).data();//Detecta a que fila hago click y me trae los datos en la variable data
    if(tablelspersonal.row(this).child.isShown()){
        var data = tablelspersonal.row(this).data();
    }
    $("#modal_editar").modal({backdrop:'static',keyboard:false})
    $("#modal_editar").modal('show');
    $("#txt_lspersonal_id").val(data.lspersonal_id);
    $("#cbm_estacion_editar").val(data.estacion_id).trigger("change");
    $("#cbm_gerencia_editar").val(data.gerencia_id).trigger("change");
    listar_departamento_combo_editar(data.gerencia_id,data.departamento_id);
    $("#cbm_estatus").val(data.lspersonal_estatus).trigger("change");
    $("#txt_nombrepersonal_editar").val(data.lspersonal_nombre);
    $("#txt_interno_editar").val(data.lspersonal_interno);
})

$('#tabla_lspersonal').on('click','.imprimir',function(){
    var data = tablelspersonal.row($(this).parents('tr')).data();//Detecta a que fila hago click y me trae los datos en la variable data
    if(tablelspersonal.row(this).child.isShown()){
        var data = tablelspersonal.row(this).data();
    }
    window.open("../vista/libreporte/reportes/generar_ticket.php?id="+parseInt(data.lspersonal_id)+"#zoom=100%","Ticket","scrollbars=NO");
})

function listar_estacion_combo(){
    $.ajax({
        "url":"../controlador/lspersonal/controlador_combo_estacion_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_estacion").html(cadena);
            $("#cbm_estacion_editar").html(cadena);
            
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_estacion").html(cadena);
            $("#cbm_estacion_editar").html(cadena);
            
        }
    })
}

function listar_gerencia_combo(){
    $.ajax({
        "url":"../controlador/lspersonal/controlador_combo_gerencia_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_gerencia").html(cadena);
            var id= $("#cbm_gerencia").val();
            listar_departamento_combo(id);
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_gerencia").html(cadena);

            
        }
    })
}

function listar_departamento_combo(id){
    $.ajax({
        "url":"../controlador/lspersonal/controlador_combo_departamento_listar.php",
        type:'POST',
        data:{
            id:id
        }
    }).done(function(resp){
        //alert(resp);
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_departamento").html(cadena);
           
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_departamento").html(cadena);

            
        }
    })
}

function Registrar_Lspersonal(){
    var idestacion = $("#cbm_estacion").val();
    var idgerencia = $("#cbm_gerencia").val();
    var iddepartamento = $("#cbm_departamento").val();
    var nombrepersonal = $("#txt_nombrepersonal").val();
    var interno = $("#txt_interno").val();
    //alert(idestacion+" - "+iddepartamento+" - "+descripcion+" - "+idgerencia+" - "+idusuario);
    if(idestacion.length==0 || idgerencia.length==0 || iddepartamento.length==0 || nombrepersonal.length==0 || interno.length==0){
        return Swal.fire("Mensaje De Advertencia", "Llene los campos vacios!", "warning");
    }

    $.ajax({
        "url": "../controlador/lspersonal/controlador_lspersonal_registro.php",
        type:'POST',
        data:{
            idestacion:idestacion,
            idgerencia:idgerencia,
            iddepartamento:iddepartamento,
            nombrepersonal:nombrepersonal,
            interno:interno
        }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                listar_lspersonal();
                limpiarcampos();
                $("#modal_registro").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Usuario actualizado", "success");
                }else{
                limpiarcampos();
                Swal.fire("Mensaje de advertencia", "El interno ya existe en la base de datos", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}


function listar_estacion_combo_editar(){
    $.ajax({
        "url":"../controlador/lspersonal/controlador_combo_estacion_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_estacion_editar").html(cadena);
           
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_estacion_editar").html(cadena);
            
        }
    })
}

function listar_gerencia_combo_editar(){
    $.ajax({
        "url":"../controlador/lspersonal/controlador_combo_gerencia_listar.php",
        type:'POST'
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_gerencia_editar").html(cadena);
            var id= $("#cbm_gerencia_editar").val();
            listar_departamento_combo_editar(id);
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_gerencia_editar").html(cadena);

            
        }
    })
}

function listar_departamento_combo_editar(id,iddepartamento){
    $.ajax({
        "url":"../controlador/lspersonal/controlador_combo_departamento_listar.php",
        type:'POST',
        data:{
            id:id
        }
    }).done(function(resp){
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                    cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_departamento_editar").html(cadena);
            if(iddepartamento!=''){
               $("#cbm_departamento_editar").val(iddepartamento).trigger("change"); 
            }
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_departamento_editar").html(cadena);

            
        }
    })
}

function Editar_Lspersonal(){
    var idpersonal = $("#txt_lspersonal_id").val();
    var idestacion = $("#cbm_estacion_editar").val();
    var idgerencia = $("#cbm_gerencia_editar").val();
    var iddepartamento = $("#cbm_departamento_editar").val();
    var estatus = $("#cbm_estatus").val();
    var nombre = $("#txt_nombrepersonal_editar").val();
    var interno = $("#txt_interno_editar").val();
    //alert(idlspersonal+" - "+idestacion+" - "+iddepartamento+" - "+descripcion+" - "+idgerencia+" - "+estatus);
    if (idestacion.length == 0 || idgerencia.length == 0 || iddepartamento.length == 0 || estatus.length == 0 || nombre.length == 0 || interno.length == 0) {
        return Swal.fire("Mensaje De Advertencia", "Llene los campos vacios!", "warning");
    }

    $.ajax({
        "url": "../controlador/lspersonal/controlador_lspersonal_editar.php",
        type:'POST',
        data:{
            idpersonal:idpersonal,
            idestacion:idestacion,
            idgerencia:idgerencia,
            iddepartamento:iddepartamento,
            estatus:estatus,
            nombre:nombre,
            interno:interno
        }
    }).done(function(resp){
         if(resp>0){
               if(resp==1){
                listar_lspersonal();
                limpiarcampos();
                $("#modal_editar").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Usuario actualizado", "success");
                }else{
                limpiarcampos();
                Swal.fire("Mensaje de advertencia", "El interno ya existe en la base de datos", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}

function limpiarcampos(){
    $("#txt_nombrepersonal").val("");
    $("#txt_interno").val("");
}

function limpiarcampos_editar(){
    $("#txt_nombrepersonal_editar").val("");
    $("#txt_interno_editar").val("");
}