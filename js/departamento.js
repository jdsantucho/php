var tabledepartamento;
function listar_departamento(){
        tabledepartamento = $("#tabla_departamento").DataTable({
        "ordering":true,
        "bLengthChange":false,
        "paging": true,
        "searching": { "regex": true },
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "pageLength": 10,
        "destroy":true,
        "async": false ,
        "processing": true,
        "ajax":{
            "url":"../controlador/departamento/controlador_departamento_listar.php",
            type:'POST'
        },
        "order":[[1, 'asc']],
        "columns":[
            {"defaultContent":""},
            {"data":"departamento_direccion"},
            {"data":"gerencia_nombre"},
            {"defaultContent":"<button style='font-size:13px;' type='button' class='editar btn btn-primary'><i class='fa fa-edit'></i></button>"}
        ],

        "language":idioma_espanol,
        "columnDefs": [
                {className: "dt-center", "targets": [0,1, 2, 3]
                }
            ],
        select: true
    });
    document.getElementById("tabla_departamento_filter").style.display="none";
    $('input.global_filter').on('keyup click',function(){
        filterGlobal();
    });
    $('input.column_filter').on('keyup click',function(){
        filterColumn($(this).parents('tr').attr('data-column'));
    });

    tabledepartamento.on('draw.dt', function(){
        var PageInfo = $('#tabla_departamento').DataTable().page.info();
        tabledepartamento.column(0, { page: 'current'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

}

$('#tabla_departamento').on('click','.editar',function(){
    var data = tabledepartamento.row($(this).parents('tr')).data();//Detecta a que fila hago click y captura los datos en la variable data
    if(tabledepartamento.row(this).child.isShown()){//Cuando esta en tamaño responsivo
        var data = tabledepartamento.row(this).data();
    }
    $("#modal_editar").modal({backdrop:'static',keyboard:false});
    $("#modal_editar").modal('show');
    $("#id_departamento").val(data.departamento_id);
    $("#txt_departamento_actual_editar").val(data.departamento_direccion);
    $("#txt_departamento_nuevo_editar").val(data.departamento_direccion);
    $("#cbm_gerencia_editar").val(data.gerencia_id).trigger("change");
})

function LimpiarCampos(){
    $("#txt_direccion").val("");
}

function filterGlobal(){
    $('#tabla_departamento').DataTable().search(
        $('#global_filter').val(),
    ).draw();
}

function listar_gerencia_combo(){
    $.ajax({
        "url":"../controlador/departamento/controlador_combo_gerencia_listar.php",
        type:'POST'
    }).done(function(resp){
        //alert(resp);
        var data = JSON.parse(resp);
        var cadena = "";
        if(data.length>0){
            for(var i=0; i < data.length; i++){
                cadena+="<option value='"+data[i][0]+"'>"+data[i][1]+"</option>";
            }
            $("#cbm_gerencia").html(cadena);
            $("#cbm_gerencia_editar").html(cadena);
            
        }else{
            cadena+="<option value=''>No se encontraron registros</option>";
            $("#cbm_gerencia").html(cadena);
            $("#cbm_gerencia_editar").html(cadena);
            
        }
    })
}

function Registrar_Departamento(){
    var direccion = $("#txt_direccion").val();
    var gerencia = $("#cbm_gerencia").val();

    if(direccion.length==0 || gerencia.length==0){
        return Swal.fire("Mensaje de advertencia", "Llene todos los campos", "warning");
    }
    $.ajax({
        "url":"../controlador/departamento/controlador_departamento_registro.php",
        type:'POST',
        data:{
            direccion:direccion,
            gerencia:gerencia
        }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                listar_departamento();
                LimpiarCampos();
                $("#modal_registro").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Departamento guardado", "success");
                }else{
                LimpiarCampos();
                Swal.fire("Mensaje de advertencia", "Ya existen", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}

function Editar_Departamento(){
    var iddepartamento = $("#id_departamento").val();
    var departamentoactual = $("#txt_departamento_actual_editar").val();
    var departamentonuevo = $("#txt_departamento_nuevo_editar").val();
    var gerencia = $("#cbm_gerencia_editar").val();
    //alert();
    if(departamentoactual.length==0 || departamentonuevo.length==0 || gerencia.length==0){
        return Swal.fire("Mensaje de advertencia", "Llene todos los campos", "warning");
    }
    $.ajax({
        "url":"../controlador/departamento/controlador_departamento_modificar.php",
        type:'POST',
        data:{
            iddepartamento:iddepartamento,
            departamentoactual:departamentoactual,
            departamentonuevo:departamentonuevo,
            gerencia:gerencia
        }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                $("#modal_editar").modal('hide');
                listar_departamento();
                Swal.fire("Mensaje de confirmacion", "Datos actualizados", "success");
                }else{
                LimpiarCampos();
                Swal.fire("Mensaje de advertencia", "Datos ya existen en la bd", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}