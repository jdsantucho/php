var tablegerencia;
function listar_gerencia(){
        tablegerencia = $("#tabla_gerencia").DataTable({
        "ordering":true,
        "bLengthChange":false,
        "paging": true,
        "searching": { "regex": true },
        "lengthMenu": [ [10, 25, 50, 100, -1], [10, 25, 50, 100, "All"] ],
        "pageLength": 10,
        "destroy":true,
        "async": false ,
        "processing": true,
        "ajax":{
            "url":"../controlador/gerencia/controlador_gerencia_listar.php",
            type:'POST'
        },
        "order":[[1, 'asc']],
        "columns":[
            {"defaultContent":""},
            {"data":"gerencia_nombre"},
            {"data":"gerencia_fregistro"},
            {"data":"gerencia_estatus",
            render: function (data, type, row ) {
                if(data=='ACTIVO'){
                    return "<span class='label label-success'>"+data+"</span>";                   
                }if(data=='INACTIVO'){
                    return "<span class='label label-danger'>"+data+"</span>";                   
                }if(data=='AGOTADO'){
                    return "<span class='label label-black' style='background:black'>"+data+"</span>";                   
                }
              }
            },  
            {"defaultContent":"<button style='font-size:13px;' type='button' class='editar btn btn-primary'><i class='fa fa-edit'></i></button>"}
        ],

        "language":idioma_espanol,
        "columnDefs": [
                {className: "dt-center", "targets": [0,1, 2, 3,4]
                }
            ],
        select: true
    });
    document.getElementById("tabla_gerencia_filter").style.display="none";
    $('input.global_filter').on('keyup click',function(){
        filterGlobal();
    });
    $('input.column_filter').on('keyup click',function(){
        filterColumn($(this).parents('tr').attr('data-column'));
    });

    tablegerencia.on('draw.dt', function(){
        var PageInfo = $('#tabla_gerencia').DataTable().page.info();
        tablegerencia.column(0, { page: 'current'}).nodes().each(function (cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
        });
    });

}

function filterGlobal(){
    $('#tabla_gerencia').DataTable().search(
        $('#global_filter').val(),
    ).draw();
}

function Registrar_Gerencia(){
    var gerencia = $("#txt_gerencia").val();
    var estatus = $("#cbm_estatus").val();


    if(gerencia.length==0 || estatus.length== 0){
        return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning");
    }

    $.ajax({
        "url":"../controlador/gerencia/controlador_gerencia_registro.php",
         type:'POST',
         data:{
            gerencia:gerencia,
            estatus:estatus
         }
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                listar_gerencia();
                LimpiarCampos();
                $("#modal_registro").modal('hide');
                Swal.fire("Mensaje de confirmacion", "Gerencia registrada", "success");
                }else{
                LimpiarCampos();
                Swal.fire("Mensaje de advertencia", "La gerencia ya existe en la base de datos", "warning");
                }
        }else{
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}

function LimpiarCampos(){
    $("#txt_gerencia").val("");
}

$('#tabla_gerencia').on('click','.editar',function(){
    var data = tablegerencia.row($(this).parents('tr')).data();//Detecta a que fila hago click y captura los datos en la variable data
    if(tablegerencia.row(this).child.isShown()){//Cuando esta en tamaño responsivo
        var data = tablegerencia.row(this).data();
    }
    $("#modal_editar").modal({backdrop:'static',keyboard:false});
    $("#modal_editar").modal('show');
    $("#id_gerencia").val(data.gerencia_id);
    $("#txt_gerencia_actual_editar").val(data.gerencia_nombre);
    $("#txt_gerencia_nueva_editar").val(data.gerencia_nombre);
    $("#cbm_estatus_editar").val(data.gerencia_estatus).trigger("change");
})

function Editar_Gerencia(){
    var id = $("#id_gerencia").val();
    var gerenciaactual = $("#txt_gerencia_actual_editar").val();
    var gerencianueva = $("#txt_gerencia_nueva_editar").val();
    var estatus = $("#cbm_estatus_editar").val();


    if(gerenciaactual.length==0 || gerencianueva.length==0 || estatus.length== 0){
        return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning");
    }

    $.ajax({
        "url":"../controlador/gerencia/controlador_gerencia_modificar.php",
         type:'POST',
         data:{
            id:id,
            espeac:gerenciaactual,
            espenu:gerencianueva,
            estatus:estatus
         },
    }).done(function(resp){
        //alert(resp);
        if(resp>0){
               if(resp==1){
                $("#modal_editar").modal('hide');
                listar_gerencia();
                Swal.fire("Mensaje de confirmacion", "Datos actualizados", "success");
                }else{
                Swal.fire("Mensaje de advertencia", "La gerencia ya existe en la base de datos", "warning");
                }
        }else{
           
            Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
        }
    })
}