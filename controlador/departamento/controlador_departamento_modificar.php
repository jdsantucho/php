<?php
    require '../../modelo/modelo_departamento.php';

    $ME = new Modelo_Departamento();
    $iddepartamento = htmlspecialchars($_POST['iddepartamento'],ENT_QUOTES,'UTF-8');
    $departamentoactual = htmlspecialchars($_POST['departamentoactual'],ENT_QUOTES,'UTF-8');
    $departamentonuevo = htmlspecialchars($_POST['departamentonuevo'],ENT_QUOTES,'UTF-8');
    $gerencia = htmlspecialchars($_POST['gerencia'],ENT_QUOTES,'UTF-8');
    $consulta = $ME->Modificar_Departamento($iddepartamento,$departamentoactual,$departamentonuevo,$gerencia);
    echo $consulta;

?>