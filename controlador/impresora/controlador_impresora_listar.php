<?php
    require '../../modelo/modelo_impresora.php';
    $MC = new Modelo_Impresora();
    $consulta = $MC->listar_impresora();
    if($consulta){
        echo json_encode($consulta);
    }else{
        echo '{
            "sEcho": 1,
            "iTotalRecords": "0",
            "iTotalDisplayRecords": "0",
            "aaData": []
        }';
    }

?>