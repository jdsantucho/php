<?php

	require_once '../../modelo/modelo_impresora.php';

	$MC = new Modelo_Impresora();
	$idimpresora = htmlspecialchars($_POST['idimpresora'], ENT_QUOTES, 'UTF-8');
	$idpropietario = htmlspecialchars($_POST['idpropietario'], ENT_QUOTES, 'UTF-8');
	$idmarca = htmlspecialchars($_POST['idmarca'], ENT_QUOTES, 'UTF-8');
	$idmodelo = htmlspecialchars($_POST['idmodelo'], ENT_QUOTES, 'UTF-8');
	$estatus = htmlspecialchars($_POST['estatus'], ENT_QUOTES, 'UTF-8');
	$macactual = htmlspecialchars($_POST['macactual'], ENT_QUOTES, 'UTF-8');
	$macnuevo = htmlspecialchars($_POST['macnuevo'], ENT_QUOTES, 'UTF-8');
	$serieactual = htmlspecialchars($_POST['serieactual'], ENT_QUOTES, 'UTF-8');
	$serienuevo = htmlspecialchars($_POST['serienuevo'], ENT_QUOTES, 'UTF-8');
	$ip = htmlspecialchars($_POST['ip'], ENT_QUOTES, 'UTF-8');
	$contacto = htmlspecialchars($_POST['contacto'], ENT_QUOTES, 'UTF-8');
	$info = htmlspecialchars($_POST['info'], ENT_QUOTES, 'UTF-8');
	$estacion = htmlspecialchars($_POST['estacion'], ENT_QUOTES, 'UTF-8');
	$gerencia = htmlspecialchars($_POST['gerencia'], ENT_QUOTES, 'UTF-8');
	$departamento = htmlspecialchars($_POST['departamento'], ENT_QUOTES, 'UTF-8');
	$consulta = $MC->Editar_Impresora($idimpresora,$idpropietario,$idmarca,$idmodelo,$estatus,$macactual,$macnuevo,$serieactual,$serienuevo,$ip,$contacto,$info,$estacion,$gerencia,$departamento);
	echo $consulta;

?>