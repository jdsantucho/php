<?php

	require_once '../../modelo/modelo_impresora.php';

	$MC = new Modelo_Impresora();
	$idpropietario = htmlspecialchars($_POST['idpropietario'], ENT_QUOTES, 'UTF-8');
	$idmarca = htmlspecialchars($_POST['idmarca'], ENT_QUOTES, 'UTF-8');
	$idmodelo = htmlspecialchars($_POST['idmodelo'], ENT_QUOTES, 'UTF-8');
	$direccionmac = htmlspecialchars($_POST['direccionmac'], ENT_QUOTES, 'UTF-8');
	$numeroserie = htmlspecialchars($_POST['numeroserie'], ENT_QUOTES, 'UTF-8');
	$direccionip = htmlspecialchars($_POST['direccionip'], ENT_QUOTES, 'UTF-8');
	$contacto = htmlspecialchars($_POST['contacto'], ENT_QUOTES, 'UTF-8');
	$info = htmlspecialchars($_POST['info'], ENT_QUOTES, 'UTF-8');
	$idestacion = htmlspecialchars($_POST['idestacion'], ENT_QUOTES, 'UTF-8');
	$idgerencia = htmlspecialchars($_POST['idgerencia'], ENT_QUOTES, 'UTF-8');
	$iddepartamento = htmlspecialchars($_POST['iddepartamento'], ENT_QUOTES, 'UTF-8');
	$estatus = htmlspecialchars($_POST['estatus'], ENT_QUOTES, 'UTF-8');
	$consulta = $MC->Registrar_Impresora($idpropietario,$idmarca,$idmodelo,$direccionmac,$numeroserie,$direccionip,$contacto,$info,$idestacion,$idgerencia,$iddepartamento,$estatus);
	echo $consulta;

?>