<?php
    require '../../modelo/modelo_interno.php';
    $ME = new Modelo_Interno();
    $consulta = $ME->listar_interno();
    if($consulta){
        echo json_encode($consulta);
    }else{
        echo '{
            "sEcho": 1,
            "iTotalRecords": "0",
            "iTotalDisplayRecords": "0",
            "aaData": []
        }';
    }

?>