<script type="text/javascript" src="../js/impresora.js?rev=<?php echo time();?>"></script>
<form autocomplete="false" onsubmit="return false">
<div class="col-md-12">
    <div class="box box-success box-solid">
        <div class="box-header with-border">
              <h3 class="box-title">IMPRESORAS</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                </button>
            </div>
              <!-- /.box-tools -->
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                <div class="col-lg-10">
                  <div class="input-group">
                   <input type="text" class="global_filter form-control" id="global_filter" placeholder="Ingresar dato a buscar">
                   <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  </div>
              </div>
              <div class="col-6">
                
                <div class="col-lg-2">
                  <button class="btn btn-danger" style="width:100%" onclick="AbrirModalRegistro()"><i class="glyphicon glyphicon-plus"></i>Nuevo registro</button><br><br>
                  </div>
              </div>

             <table id="tabla_impresora" class="display responsive nowrap cell-border" style="width:100%">
                <thead style="text-align: center;">
                  <tr>
                    <th>#</th>
                    <th>IP</th>
                    <th>Modelo</th>
                    <th>Departamento</th>
                    <th>Contacto</th>
                    <th>Estacion</th>
                    <th>Serie</th>
                    <th>Acci&oacute;n</th>
                    <th>Estatus</th>
                    <th>MAC</th>
                    <th>Propietario</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>IP</th>
                    <th>Modelo</th>
                    <th>Departamento</th>
                    <th>Contacto</th>
                    <th>Estacion</th>
                    <th>Serie</th>
                    <th>Acci&oacute;n</th>
                    <th>Estatus</th>
                    <th>MAC</th>
                    <th>Propietario</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
    </div>
          <!-- /.box -->
</div>
</form>
<div class="modal fade" id="modal_registro" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Registro de impresora</b></h4>
        </div>
        <div class="modal-body">
          <div class="row">
          <div class="col-lg-12" style="text-align:center">
            <b>DATOS DE REGISTRO</b><br><br>
          </div>
          <div class="col-lg-4">
            <label for="">Propietario:</label>
            <select class="js-example-basic-single" name="state" id="cbm_propietario" style="width:100%;">
            </select><br><br>
          </div>
       
          <div class="col-lg-4">
            <label for="">Marca:</label>
            <select class="js-example-basic-single" name="state" id="cbm_marca" style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Modelo:</label>
            <select class="js-example-basic-single" name="state" id="cbm_modelo" style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Direccion MAC:</label>
            <input type="text" class="form-control" id="txt_direccionmac" placeholder="Ingrese direccion mac" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>

          <div class="col-lg-4">
            <label for="">Numero de serie:</label>
            <input type="text" class="form-control" id="txt_numeroserie" placeholder="Ingrese numero de serie" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>

          <div class="col-lg-4">
            <label for="">Estado:</label>
            <select class="js-example-basic-single" name="state" id="cbm_estatus" style="width:100%;">
              <option value="LIBRE">LIBRE</option>
              <option value="REPARAR">REPARAR</option>
              <option value="INSTALADA">INSTALADA</option>
            </select><br><br>
          </div>

          <div class="col-lg-12" style="text-align:center">
            <b>DATOS DE INSTALACION</b><br><br>
          </div>
          <div class="col-lg-4">
            <label for="">Direccion IP:</label>
            <input type="text" class="form-control" id="txt_direccionip" placeholder="Ingrese numero IP" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>
          
          <div class="col-lg-4">
            <label for="">Responsable/Interno contacto:</label>
            <input type="text" class="form-control" id="txt_contacto" placeholder="Ingrese contacto" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>

          <div class="col-lg-4">
            <label for="">Info:</label>
            <input type="text" class="form-control" id="txt_info" placeholder="Detalles" maxlength="250" onkeypress="return soloLetras(event)"><br>
          </div>

          <div class="col-lg-4">
            <label for="">Estacion:</label>
            <select class="js-example-basic-single" name="state" id="cbm_estacion" style="width:100%;">
            </select><br><br>
          </div>
       
          <div class="col-lg-4">
            <label for="">Gerencia:</label>
            <select class="js-example-basic-single" name="state" id="cbm_gerencia" style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Departamento:</label>
            <select class="js-example-basic-single" name="state" id="cbm_departamento" style="width:100%;">
            </select><br><br>
          </div>


          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Registrar_Impresora()"><i class="fa fa-check"> <b>Registrar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="modal_editar" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Editar de impresora</b></h4>
        </div>
        <div class="modal-body">
          <div class="row">
        <div class="col-lg-12" style="text-align:center">
            <b>DATOS DE REGISTRO</b><br><br>
          </div>
          <div class="col-lg-4">
            <input type="text" id="txt_impresora_id" hidden>
            <label for="">Propietario:</label>
            <select class="js-example-basic-single" name="state" id="cbm_propietario_editar" style="width:100%;">
            </select><br><br>
          </div>
       
          <div class="col-lg-4">
            <label for="">Marca:</label>
            <select class="js-example-basic-single" name="state" id="cbm_marca_editar" style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Modelo:</label>
            <select class="js-example-basic-single" name="state" id="cbm_modelo_editar" style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Direccion MAC:</label>
            <input type="text" id="txt_mac_actual_editar" maxlength="50" onkeypress="return soloLetras(event)" hidden>
            <input type="text" class="form-control" id="txt_mac_nuevo_editar" placeholder="Ingrese insumo medico" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>

          <div class="col-lg-4">
            <label for="">Numero de serie:</label>
            <input type="text" id="txt_serie_actual_editar" onkeypress="return soloLetras(event)" hidden>
            <input type="text" class="form-control" id="txt_serie_nuevo_editar"maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>


          <div class="col-lg-4">
            <label for="">Estatus:</label>
            <select class="js-example-basic-single" name="state" id="cbm_estatus_editar" style="width:100%;">
              <option value="INSTALADA">INSTALADA</option>
              <option value="LIBRE">LIBRE</option>
              <option value="REPARAR">REPARAR</option>
            </select><br><br>
          </div>

          <div class="col-lg-12" style="text-align:center">
            <b>DATOS DE INSTALACION</b><br><br>
          </div>
          <div class="col-lg-4">
            <label for="">Direccion IP:</label>
            <input type="text" class="form-control" id="txt_direccionip_editar" placeholder="Ingrese numero IP" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>
          
          <div class="col-lg-4">
            <label for="">Responsable/Interno contacto:</label>
            <input type="text" class="form-control" id="txt_contacto_editar" placeholder="Ingrese contacto" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>

          <div class="col-lg-4">
            <label for="">Info:</label>
            <input type="text" class="form-control" id="txt_info_editar"  maxlength="250" onkeypress="return soloLetras(event)"><br>
          </div>

          <div class="col-lg-4">
            <label for="">Estacion:</label>
            <select class="js-example-basic-single" name="state" id="cbm_estacion_editar" style="width:100%;">
            </select><br><br>
          </div>
       
          <div class="col-lg-4">
            <label for="">Gerencia:</label>
            <select class="js-example-basic-single" name="state" id="cbm_gerencia_editar" style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Departamento:</label>
            <select class="js-example-basic-single" name="state" id="cbm_departamento_editar" style="width:100%;">
            </select><br><br>
          </div>



          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Editar_Impresora()"><i class="fa fa-check"> <b>Editar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="modal_detalles" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Detalles</b></h4>
        </div>
        <div class="modal-body">
          <div class="row">
        <div class="col-lg-12" style="text-align:center">
            <b>DATOS DE REGISTRO</b><br><br>
          </div>
          <div class="col-lg-4">
            <input type="text" id="txt_impresora_id_detalle" hidden>
            <label for="">Propietario:</label>
            <select class="js-example-basic-single" name="state" id="cbm_propietario_detalle" disabled style="width:100%;">
            </select><br><br>
          </div>
       
          <div class="col-lg-4">
            <label for="">Marca:</label>
            <select class="js-example-basic-single" name="state" id="cbm_marca_detalle" disabled style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Modelo:</label>
            <select class="js-example-basic-single" name="state" id="cbm_modelo_detalle" disabled style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Direccion MAC:</label>
            <input type="text" class="form-control" id="txt_mac_nuevo_detalle" disabled><br>
          </div>

          <div class="col-lg-4">
            <label for="">Numero de serie:</label>
            <input type="text" class="form-control" id="txt_serie_nuevo_detalle" disabled><br>
          </div>


          <div class="col-lg-4">
            <label for="">Estatus:</label>
            <select class="js-example-basic-single" name="state" id="cbm_estatus_detalle"  disabled style="width:100%;">
              <option value="LIBRE">LIBRE</option>
              <option value="INSTALADA">INSTALADA</option>
              <option value="REPARAR">REPARAR</option>
            </select><br><br>
          </div>

          <div class="col-lg-12" style="text-align:center">
            <b>DATOS DE INSTALACION</b><br><br>
          </div>
          <div class="col-lg-4">
            <label for="">Direccion IP:</label>
            <input type="text" class="form-control" id="txt_direccionip_detalle" disabled><br>
          </div>
          
          <div class="col-lg-4">
            <label for="">Responsable/Interno contacto:</label>
            <input type="text" class="form-control" id="txt_contacto_detalle" disabled><br>
          </div>

          <div class="col-lg-4">
            <label for="">Info:</label>
            <input type="text" class="form-control" id="txt_info_detalle" disabled><br>
          </div>

          <div class="col-lg-4">
            <label for="">Estacion:</label>
            <select class="js-example-basic-single" name="state" id="cbm_estacion_detalle" disabled style="width:100%;">
            </select><br><br>
          </div>
       
          <div class="col-lg-4">
            <label for="">Gerencia:</label>
            <select class="js-example-basic-single" name="state" id="cbm_gerencia_detalle" disabled style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Departamento:</label>
            <select class="js-example-basic-single" name="state" id="cbm_departamento_detalle" disabled style="width:100%;">
            </select><br><br>
          </div>



          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>


<script>
$(document).ready(function(){
  listar_impresora();
  listar_propietario_combo();
  listar_propietario_combo_editar();
  listar_marca_combo();
  listar_marca_combo_editar();
  listar_estacion_combo();
  listar_estacion_combo_editar();
  listar_gerencia_combo();
  listar_gerencia_combo_editar();

  $("#cbm_marca").change(function(){
    var idmarca=$("#cbm_marca").val();
    listar_modelo_combo(idmarca);
  });

  $("#cbm_marca_editar").change(function(){
   var idmarca=$("#cbm_marca_editar").val();
    listar_modelo_combo_editar(idmarca,'');
  });

  $("#cbm_gerencia").change(function(){
    var idgerencia=$("#cbm_gerencia").val();
    listar_departamento_combo(idgerencia);
  });

  $("#cbm_gerencia_editar").change(function(){
   var idgerencia=$("#cbm_gerencia_editar").val();
    listar_departamento_combo_editar(idgerencia,'');
  });
    
    $('.js-example-basic-single').select2();
    $("#modal_registro").on('shown.bs.modal',function(){
      $("#cbm_estacion").focus();
    })
    

});


        


$('.box').boxWidget({
  animationSpeed : 500,
  collapseTrigger : '[data-widget="collapse"]',
  removeTrigger : '[data-widget="remove"]',
  collapseIcon : 'fa-minus',
  expandIcon : 'fa-plus',
  removeIcon : 'fa-times'
})

</script>