<script type="text/javascript" src="../js/lspersonal.js?rev=<?php echo time();?>"></script>
<form autocomplete="false" onsubmit="return false">
<div class="col-md-12">
    <div class="box box-success box-solid">
        <div class="box-header with-border">
              <h3 class="box-title">USUARIOS - INTERNOS</h3>

              <!-- /.box-tools -->
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                <div class="col-lg-10">
                  <div class="input-group">
                   <input type="text" class="global_filter form-control" id="global_filter" placeholder="Ingresar dato a buscar">
                   <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  </div>
              </div>
                <div class="col-lg-2">
                  <button class="btn btn-danger" style="width:100%" onclick="AbrirModalRegistro()"><i class="glyphicon glyphicon-plus"></i>Nuevo registro</button>
                  </div>
              </div><br><br>
              <table id="tabla_lspersonal" class="display responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Interno</th>
                    <th>Departamento</th>
                    <th>Gerencia</th>
                    <th>Estacion</th>
                    <th>Estatus</th>
                    <th>Acci&oacute;n</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Interno</th>
                    <th>Departamento</th>
                    <th>Gerencia</th>
                    <th>Estacion</th>
                    <th>Estatus</th>
                    <th>Acci&oacute;n</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
    </div>
          <!-- /.box -->
</div>
</form>
<div class="modal fade" id="modal_registro" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Registro de lspersonal</b></h4>
        </div>
        <div class="modal-body">
          <div class="row">
        
          <div class="col-lg-4">
            <label for="">Estacion:</label>
            <select class="js-states form-control" name="state" id="cbm_estacion" style="width:100%;">
            </select><br><br>
          </div>
       
          <div class="col-lg-4">
            <label for="">Gerencia:</label>
            <select class="js-states form-control" name="state" id="cbm_gerencia" style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Departamento:</label>
            <select class="js-states form-control" name="state" id="cbm_departamento" style="width:100%;">
            </select><br><br>
          </div>


          <div class="col-lg-4">
            <label for="">Nombre:</label>
            <input type="text" class="form-control" id="txt_nombrepersonal" placeholder="Ingrese usuario" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>


          <div class="col-lg-4">
            <label for="">Numero interno:</label>
            <input type="text" class="form-control" id="txt_interno" placeholder="Ingrese numero interno" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>

          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Registrar_Lspersonal()"><i class="fa fa-check"> <b>Registrar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="modal_editar" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Editar de lspersonal</b></h4>
        </div>
        <div class="modal-body">
          <div class="row">
        
          <div class="col-lg-4">
            <input type="text" id="txt_lspersonal_id" hidden>
            <label for="">Estacion:</label>
            <select class="js-states form-control" name="state" id="cbm_estacion_editar" style="width:100%;">
            </select><br><br>
          </div>
       
          <div class="col-lg-4">
            <label for="">Gerencia:</label>
            <select class="js-states form-control" name="state" id="cbm_gerencia_editar" style="width:100%;">
            </select><br><br>
          </div>

          <div class="col-lg-4">
            <label for="">Departamento:</label>
            <select class="js-states form-control" name="state" id="cbm_departamento_editar" style="width:100%;">
            </select><br><br>
          </div>
          <div class="col-lg-4">
            <label for="">Estatus:</label>
            <select class="js-states form-control" name="state" id="cbm_estatus" style="width:100%;">
              <option value="ACTIVO">ACTIVO</option>
              <option value="INACTIVO">INACTIVO</option>
            </select><br><br>
          </div>

           <div class="col-lg-4">
            <label for="">Nombre:</label>
            <input type="text" class="form-control" id="txt_nombrepersonal_editar" placeholder="Ingrese usuario" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>


          <div class="col-lg-4">
            <label for="">Numero interno:</label>
            <input type="text" class="form-control" id="txt_interno_editar" placeholder="Ingrese numero interno" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>


          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Editar_Lspersonal()"><i class="fa fa-check"> <b>Editar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>

<script>
$(document).ready(function(){
  listar_lspersonal();
  listar_estacion_combo();
  listar_gerencia_combo();
  listar_estacion_combo_editar();
  listar_gerencia_combo_editar();
  $("#cbm_gerencia").change(function(){
    var id=$("#cbm_gerencia").val();
    listar_departamento_combo(id);
  });
  $("#cbm_gerencia_editar").change(function(){
    var idgerencia=$("#cbm_gerencia_editar").val();
    listar_departamento_combo_editar(idgerencia,'');
  });
    $('.js-states form-control').select2();
    $("#modal_registro").on('shown.bs.modal',function(){
      $("#cbm_estacion").focus();
    })
});

$('.box').boxWidget({
  animationSpeed : 500,
  collapseTrigger : '[data-widget="collapse"]',
  removeTrigger : '[data-widget="remove"]',
  collapseIcon : 'fa-minus',
  expandIcon : 'fa-plus',
  removeIcon : 'fa-times'
})



</script>