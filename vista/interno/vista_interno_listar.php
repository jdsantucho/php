<script type="text/javascript" src="../js/interno.js?rev=<?php echo time();?>"></script>
<div class="col-md-12">
    <div class="box box-success box-solid">
        <div class="box-header with-border">
              <h3 class="box-title">INTERNOS</h3>

              <!-- /.box-tools -->
        </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="form-group">
                <div class="col-lg-10">
                  <div class="input-group">
                   <input type="text" class="global_filter form-control" id="global_filter" placeholder="Ingresar dato a buscar">
                   <span class="input-group-addon"><i class="fa fa-search"></i></span>
                  </div>
              </div>
                <div class="col-lg-2">
                  <button class="btn btn-danger" style="width:100%" onclick="AbrirModalRegistro()"><i class="glyphicon glyphicon-plus"></i>Nuevo registro</button><br><br>
                  </div>
              </div>
              <table id="tabla_interno" class="display responsive nowrap" style="width:100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Interno</th>
                    <th>Acci&oacute;n</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th>Interno</th>
                    <th>Acci&oacute;n</th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
    </div>
          <!-- /.box -->
</div>

<div class="modal fade" id="modal_registro" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Registro de interno</b></h4>
        </div>
        <div class="row">
        <div class="modal-body">
          <div class="col-lg-12">
            <label for="">Contacto:</label>
            <input type="text" class="form-control" id="txt_interno" placeholder="Ingrese nombre del contacto" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>
          <div class="col-lg-12">
            <label for="">Interno:</label>
            <input type="text" class="form-control" id="txt_numinterno" placeholder="Ingrese numero de interno" maxlength="5" onkeypress="return soloLetras(event)"><br>
          </div>
        </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Registrar_Interno()"><i class="fa fa-check"> <b>Registrar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="modal_editar" role="dialog">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header bg-primary">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style="text-align: center;"><b>Editar de interno</b></h4>
        </div>
        <div class="row">
        <div class="modal-body">
          <div class="col-lg-12">
            <input type="text" id="id_interno" hidden>
            <label for="">Nombre:</label>
            <input type="text" id="txt_interno_actual_editar" placeholder="Ingrese interno" maxlength="50" onkeypress="return soloLetras(event)" hidden>
            <input type="text" class="form-control" id="txt_interno_nueva_editar" placeholder="Ingrese interno" maxlength="50" onkeypress="return soloLetras(event)"><br>
          </div>
          <div class="col-lg-12">
            <label for="">Interno:</label>
            <input type="text" class="form-control" id="txt_numinterno_editar" maxlength="5" onkeypress="return soloLetras(event)"><br>
          </div>
        </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-primary" onclick="Editar_Interno()"><i class="fa fa-check"> <b>Editar</b></i></button>
          <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"> <b>Cerrar</b></i></button>
        </div>
      </div>
    </div>
  </div>

<script>
$(document).ready(function(){
  listar_interno();
    $('.js-example-basic-single').select2();
    $("#modal_registro").on('shown.bs.modal',function(){
      $("#txt_interno").focus();
    })
});

$('.box').boxWidget({
  animationSpeed : 500,
  collapseTrigger : '[data-widget="collapse"]',
  removeTrigger : '[data-widget="remove"]',
  collapseIcon : 'fa-minus',
  expandIcon : 'fa-plus',
  removeIcon : 'fa-times'
})


</script>