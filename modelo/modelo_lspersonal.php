<?php
    class Modelo_Lspersonal{
        private $conexion;
        function __construct(){
            require_once 'modelo_conexion.php';
            $this->conexion = new conexion();
            $this->conexion->conectar();
        }

        function Registrar_Lspersonal($idestacion,$idgerencia,$iddepartamento,$nombrepersonal,$interno){
            $sql = "call SP_REGISTRAR_LSPERSONAL('$idestacion','$idgerencia','$iddepartamento','$nombrepersonal','$interno')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }
        
        function listar_lspersonal(){
            $sql = "call SP_LISTAR_LSPERSONAL()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_assoc($consulta)) {
                    $arreglo["data"][]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }


        function Editar_Lspersonal($idpersonal,$idestacion,$idgerencia,$iddepartamento,$estatus,$nombre,$interno){
            $sql = "call SP_MODIFICAR_LSPERSONAL('$idpersonal','$idestacion','$idgerencia','$iddepartamento','$estatus','$nombre','$interno')";
            if ($consulta = $this->conexion->conexion->query($sql)){
                    return 1;    
                }else{
                    return 0;
                }
                $this->conexion->cerrar();
        }

        function listar_estacion_combo(){
            $sql = "call SP_LISTAR_ESTACION_COMBO()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

        function listar_gerencia_combo(){
            $sql = "call SP_LISTAR_GERENCIA_COMBO()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }
        
        function listar_departamento_combo($id){
            $sql = "call SP_LISTAR_DEPARTAMENTO_COMBO('$id')";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }


    }
?>