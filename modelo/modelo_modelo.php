<?php
    class Modelo_Modelo{
        private $conexion;
        function __construct(){
            require_once 'modelo_conexion.php';
            $this->conexion = new conexion();
            $this->conexion->conectar();
        }

        
        function listar_modelo(){
            $sql = "call SP_LISTAR_MODELO()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_assoc($consulta)) {
                    $arreglo["data"][]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

        function Registrar_Modelo($modelo,$marca){
            $sql = "call SP_REGISTRAR_MODELO('$modelo','$marca')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }

        function Modificar_Modelo($idmodelo,$modeloactual,$modelonuevo,$marca){
            $sql = "call SP_MODIFICAR_MODELO('$idmodelo','$modeloactual','$modelonuevo','$marca')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }


        function listar_marca_combo(){
            $sql = "call SP_LISTAR_COMBO_MARCA()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

    }

?>