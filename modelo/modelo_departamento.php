<?php
    class Modelo_Departamento{
        private $conexion;
        function __construct(){
            require_once 'modelo_conexion.php';
            $this->conexion = new conexion();
            $this->conexion->conectar();
        }

        
        function listar_departamento(){
            //$sql = "call SP_LISTAR_MEDICO()";
            $sql = "call SP_LISTAR_DEPARTAMENTO()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_assoc($consulta)) {
                    $arreglo["data"][]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

        function Registrar_Departamento($direccion,$gerencia){
            //$sql = "call SP_REGISTRAR_MEDICO('$direccion','$gerencia')";
            $sql = "call SP_REGISTRAR_DEPARTAMENTO('$direccion','$gerencia')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }

        function Modificar_Departamento($iddepartamento,$departamentoactual,$departamentonuevo,$gerencia){
            $sql = "call SP_MODIFICAR_DEPARTAMENTO('$iddepartamento','$departamentoactual','$departamentonuevo','$gerencia')";
            //$sql = "call SP_MODIFICAR_MEDICO('$iddepartamento','$departamentoactual','$departamentonuevo','$gerencia')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }


        function listar_gerencia_combo(){
            $sql = "call SP_LISTAR_COMBO_GERENCIA()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

    }

?>