<?php
    class Modelo_Impresora{
        private $conexion;
        function __construct(){
            require_once 'modelo_conexion.php';
            $this->conexion = new conexion();
            $this->conexion->conectar();
        }

        function Registrar_Impresora($idpropietario,$idmarca,$idmodelo,$direccionmac,$numeroserie,$direccionip,$contacto,$info,$idestacion,$idgerencia,$iddepartamento,$estatus){
            $sql = "call SP_REGISTRAR_IMPRESORA('$idpropietario','$idmarca','$idmodelo','$direccionmac','$numeroserie','$direccionip','$contacto','$info','$idestacion','$idgerencia','$iddepartamento','$estatus')";
            if ($consulta = $this->conexion->conexion->query($sql)) {
                if ($row = mysqli_fetch_array($consulta)) {
                        return $id = trim($row[0]);//Devuelve valores
                }
                $this->conexion->cerrar();
            }
        }
        
        function listar_impresora(){
            $sql = "call SP_LISTAR_IMPRESORA()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_assoc($consulta)) {
                    $arreglo["data"][]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

        function Editar_Impresora($idimpresora,$idpropietario,$idmarca,$idmodelo,$estatus,$macactual,$macnuevo,$serieactual,$serienuevo,$ip,$contacto,$info,$estacion,$gerencia,$departamento){
            $sql = "call SP_MODIFICAR_IMPRESORA('$idimpresora','$idpropietario','$idmarca','$idmodelo','$estatus','$macactual','$macnuevo','$serieactual','$serienuevo','$ip','$contacto','$info','$estacion','$gerencia','$departamento')";
            if ($consulta = $this->conexion->conexion->query($sql)){
                    return 1;    
                }else{
                    return 0;
                }
                $this->conexion->cerrar();
        }

        function listar_propietario_combo(){
            $sql = "call SP_LISTAR_PROPIETARIO_COMBO()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

        function listar_marca_combo(){
            $sql = "call SP_LISTAR_MARCA_COMBO()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }
        
        function listar_modelo_combo($id){
            $sql = "call SP_LISTAR_MODELO_COMBO('$id')";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

        function listar_estacion_combo(){
            $sql = "call SP_LISTAR_ESTACION_COMBO()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

        function listar_gerencia_combo(){
            $sql = "call SP_LISTAR_GERENCIA_COMBO()";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }
        
        function listar_departamento_combo($id){
            $sql = "call SP_LISTAR_DEPARTAMENTO_COMBO('$id')";
            $arreglo = array();
            if ($consulta = $this->conexion->conexion->query($sql)) {
                while ($consulta_VU = mysqli_fetch_array($consulta)) {
                    $arreglo[]=$consulta_VU;
                    
                }
                return $arreglo;
                $this->conexion->cerrar();
            }
        }

    }
?>